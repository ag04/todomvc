import Vue from 'vue';
import Vuex from 'vuex';
import VueResource from 'vue-resource';
import axios from 'axios';
Vue.use(Vuex);
Vue.use(VueResource);

export default new Vuex.Store({
  state: {
    taskList: []
  },
  mutations: {
    storeTaskList (state, list) {
      state.taskList = list;
    }
  },
  actions: {
    fetchTaskList({ commit }) {
      axios
        .get('https://todomvc-894af.firebaseio.com/tasks.json')
        .then(response => {
          commit('storeTaskList', response.data)
        });
    },
    addTask({ commit, state }, task) {
      axios
        .post('https://todomvc-894af.firebaseio.com/tasks.json', task)
        .then(response => {
          this.dispatch('fetchTaskList');
        }).catch(error => console.log(error));
    },
    updateTaskList({ commit, state }, task) {
      for (let key in state.taskList) {
        if (state.taskList[key].id === task.id) {
          axios
            .put('https://todomvc-894af.firebaseio.com/tasks/' + key + '.json', task)
            .then(response => {
              console.log(response);
            }).catch(error => console.log(error));
        }
      }
    },
    removeFromTaskList({ commit, state }, task) {
      for (let key in state.taskList) {
        if (state.taskList[key].id === task.id) {
          axios
            .delete('https://todomvc-894af.firebaseio.com/tasks/' + key + '.json', task)
            .then(response => {
              console.log(response);
              this.dispatch('fetchTaskList');
            }).catch(error => console.log(error));
        }
      }
    }
  },
  getters: {
    taskList (state) {
      return state.taskList;
    }
  }
});
