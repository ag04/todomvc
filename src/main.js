import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import './registerServiceWorker'
import BootstrapVue from 'bootstrap-vue/dist/bootstrap-vue.esm'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import 'vue-material-design-icons/styles.css'
import VueUniqIds from 'vue-uniq-ids'
import VueResource from 'vue-resource'
import VueAxios from 'vue-axios'

Vue.use(BootstrapVue)
Vue.use(VueUniqIds)
Vue.use(VueResource)
Vue.use(VueAxios)
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
