# TODOMVC by Mia Matic 
The purpose of this project is to improve Vue skills. The idea is to 
reconstruct design and functionalities of the page 
```http://todomvc.com/examples/vue/```. It is mentored by Nikola Masic.
## Project setup
1. Clone repository: git clone ```git@bitbucket.org:ag04/todomvc.git ```

2. Run ```
       npm install
       ```

### Compiles and minifies for production
```
npm run build
```


### Running application
Run
```
npm run serve
```
